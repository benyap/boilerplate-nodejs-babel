# Boilerplate - Nodejs with Babel

This is a project which contains all the boilerplate code necessary to kick-start a project in Nodejs with Babel transpilation. Comes preinstalled with the following: 

- `Babel` build and watch scripts

- `ESLint` to ensure a consistent style across the code

- `Jest` for testing

- `commitlint` to ensure good commit messages

- `.gitlab-ci.yml` sample configuration to help set up CI/CD


## Project

### Setting up your environment

```bash
# Clone the repository to your local machine
git clone git@gitlab.com:bwyap/boilerplate-nodejs-babel.git

# Step into local repo
cd boilerplate-nodejs-babel

# Install dependencies 
npm install
```

### Running the project

To run the repository locally, ensure you have the following environment variables in `.env` in the root directory, using actual values. 
**Do not commit this file.**

```ini
NODE_ENV=
```

Then use the following command to start the project

```bash
npm run local
```

Your code will be transpiled using `babel` and watched for changes. 

### Building the project

This command will build the project and output it to the `dist` directory

```bash
npm run build
```

You can then run the build using the following command

```bash
npm start
```

## Contributing

There are a number of conventions that should be followed when developing this project. 
Please see the [CONTRIBUTING](CONTRIBUTING.md) guide for more details. 


## License

This project is licensed under the Apache License, Version 2.0.

See the [LICENSE](LICENSE) for more details. 
