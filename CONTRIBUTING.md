# Contributing guide

Please follow the following conventions when contributing to this project on Gitlab. 

## Committing

Commit messages are linted using `commitlint` and is automatically checked when you use `git commit -m "<YOUR MESSAGE>"`. 
They must have the following format:

```text
TYPE: subject
body?
footer?
```

Where `TYPE` is one of the following: 

```
build              Affects the build system or external dependencies.
ci                 Changes CI configuration files and scripts.
docs               Adds or alters documentation.
feat               Adds a new feature.
fix                Solves a bug.
refactor           Rewrites code without feature, performance or bug changes.
revert             Reverts a previous commit.
style              Improves formatting, white-space.
test               Adds or modifies tests.
```

And the body of the commit message contains a more detailed description of the changes made in the commit, and **why they were made**. 
The body and footer of the commit message are optional. 

You can use the following prompt to help guide you through your submitting your commit message.

```bash
npm run commit
```

Type `:skip` if you wish to skip entering the body and footer sections. 


## Style & Linting

This codebase adheres to the [Airbnb Styleguide](https://github.com/airbnb/javascript) and is enforced using [ESLint](https://eslint.org/).

Commits and pushes are blocked if your code does not pass linting. 
Check if your source code is compliant by running: 

```bash
npm run lint
```


## Running tests

Use the following command to run tests. 
All tests must pass before pushing to the repository. 

```bash
npm run test
```

Each module should have its own unit test. 
Unit test files should be named using the format `UNIT.spec.js`,
where `UNIT` is the name of the unit being tested. 


## Pushing 

The following pre-push checks are automatically run when you use `git push`: 

1. Code linting must pass (see the Style & Linting section)

2. Tests must pass (see the Running tests section)

If these conditions fail, your push will be rejected. 


## Branching 

All new branches should be created off the latest commit off the `develop` branch. 
Feature branch names should follow the format `feature/TYPE-DESCRIPTION`, 
where `TYPE` is one of: 

- `feat` - for adding new features

- `fix` - for bug fixes

- `config` - for modifying development configuration

and `DESCRIPTION` is a short phrase which describes the feature. 

Hotfix branches can be created off the `staging` or `master` branch **only if** a critical issue needs to be addressed, and should follow the format `hotfix/DESCRIPTION`. 
Non-critical issues should be released through the `develop` branch as a fix. 


## Merge requests

The following branches are protected and can only be merged using a merge request: 

- `develop`

- `staging`

- `master`

Only the `develop` branche should only be merged into `staging`,
and only the `staging` branch should be merged into `master`. 
If needed, `hotfix/*` branches may also be merged into `staging` or `master`. 

**Merge requests must pass linting and tests before merging.** 
This is enforced using Gitlab pipelines.
If the pipeline fails, the merge request is rejected. 
Branches are configured to automatically deploy to their respective environments once a merge is successful using Gitlab CI/CD,
so it is important that these branches are kept in a stable, deployable state. 
